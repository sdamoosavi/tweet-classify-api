# pull official base image
FROM python:3.7.0-alpine

WORKDIR /app/

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install psycopg2 dependencies
RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev

# install dependencies
RUN pip install --upgrade pip
COPY ./requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt

# copy project
COPY . app/

# run entrypoint.sh
COPY ./entrypoint.sh /app/entrypoint.sh
CMD ["/entrypoint.sh"]

RUN adduser -D user
RUN chown user:user -R /app/
USER user